<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/9/18
 * Time: 8:20 PM
 */

return [

    // main touts
    '' => [
        'controller' => 'main',
        'action' => 'index',
    ],

    '{id:\d+}' => [
        'controller' => 'main',
        'action' => 'index',
    ],

    'delete/{id:\d+}' => [
        'controller' => 'main',
        'action' => 'delete',
    ],

    'update/{id:\d+}' => [
        'controller' => 'main',
        'action' => 'update',
    ],

    // for sorting

    'login-asc'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    'login-desc'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    'date-asc'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    'date-desc'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    'email-asc'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    'email-desc'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    'status-asc'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    'status-desc'  => [
        'controller' => 'main',
        'action' => 'sort',
    ],

    //account routs

    'account' => [
        'controller' => 'account',
        'action' => 'index',
    ],

    'registration' => [
        'controller' => 'account',
        'action' => 'registration',
    ],

    'login' => [
        'controller' => 'account',
        'action' => 'login',
    ],

    'logout' => [
        'controller' => 'account',
        'action' => 'logout',
    ],

];

//dir-asc/order-price/p-2/